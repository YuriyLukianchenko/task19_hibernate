package com.lukianchenko.model;

import javax.persistence.*;

@Entity
@Table(name = "currencies", schema = "hibernate_testing")
public class CurrencyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private String id;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CurrencyEntity{" +
                "id='" + id + '\'' +
                '}';
    }
}
