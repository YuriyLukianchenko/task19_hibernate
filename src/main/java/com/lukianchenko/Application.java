package com.lukianchenko;

import com.lukianchenko.model.CurrencyEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;



public class Application {
    private static SessionFactory ourSessionFactory;

    static {
        try { // Create the SessionFactory from hibernate.cfg.xml
            ourSessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession(); //return opened session
    }

    public static void main(final String[] args) throws Exception {
        // get opened session
        Session session = getSession();

        try {
            session.beginTransaction();
            CurrencyEntity currency = new CurrencyEntity();
            currency.setId("default");
            session.save(currency);//here bug
            session.getTransaction().commit();


            ReadCurrencyTable(session);

        } finally {
            session.close();
            System.exit(0);
        }
    }

    private static void ReadCurrencyTable(Session session) {
        CurrencyEntity currency1 = null;

        Query query = session.createQuery("from " + "CurrencyEntity");

        System.out.format("\nTable Currency --------------------\n");
        System.out.format("%3s \n", "ID");
        
        for (Object obj : query.list()) {

             currency1 = (CurrencyEntity) obj;

            System.out.format("%3s \n", currency1.getId());

        }
    }


}

